# protoc-gen-go-rsn
Proof of concept generation of resource reference structs.

To generate the example, perform the following steps:
```bash
go build -o protoc-gen-go-rsn .
cd example
protoc -I . -I ~/dev/googleapis/ \
  --plugin=../protoc-gen-go-rsn \
  --go-rsn_out=. \
  --go-rsn_opt=module=gitlab.com/simon.bos/protoc-gen-go-rsn/example \
  --go-rsn_opt="Mmessage.proto=gitlab.com/simon.bos/protoc-gen-go-rsn/example/messagingrsn;messagingrsn" \
  message.proto

```

Note: change ~/dev/googleapis to a destination where you cloned [googleapis](https://github.com/googleapis/googleapis).
