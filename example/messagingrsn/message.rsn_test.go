package messagingrsn_test

import (
	"fmt"
	"log"

	"gitlab.com/simon.bos/protoc-gen-go-rsn/example/messagingrsn"
)

func ExampleParseMessageParentResourceName() {
	resourceName := "users/abc/contexts/bef"
	messageParentRef, err := messagingrsn.ParseMessageParentResourceName(resourceName)
	if err != nil {
		log.Fatalf("error parsing Message resource name: %v", err)
	}

	// Type check to access pattern-specific fields
	switch messageParentRef.Type {
	case messagingrsn.UserMessageParentType:
		fmt.Println("User Id:", messageParentRef.UserId)
	case messagingrsn.UserContextMessageParentType:
		fmt.Println("User Id:", messageParentRef.UserId)
		fmt.Println("Context Id:", messageParentRef.ContextId)
	case messagingrsn.OrganisationMessageParentType:
		fmt.Println("Organisation Id:", messageParentRef.OrganisationId)
	case messagingrsn.OrganisationSharedMailboxMessageParentType:
		fmt.Println("Organisation Id:", messageParentRef.OrganisationId)
		fmt.Println("SharedMailbox Id:", messageParentRef.SharedMailboxId)
	default:
		fmt.Println("Unknown MessageRef parent type")
	}

	outResourceName := messageParentRef.ResourceName()
	if resourceName != outResourceName {
		log.Fatalf("got %v, want %v", outResourceName, resourceName)
	}
}

func ExampleParseMessageResourceName() {
	resourceName := "organisations/abc/messages/bef"
	messageRef, err := messagingrsn.ParseMessageResourceName(resourceName)
	if err != nil {
		log.Fatalf("error parsing Message resource name: %v", err)
	}

	// Type check to access pattern-specific fields
	switch messageRef.Parent.Type {
	case messagingrsn.UserMessageParentType:
		fmt.Println("User Id:", messageRef.Parent.UserId)
	case messagingrsn.UserContextMessageParentType:
		fmt.Println("User Id:", messageRef.Parent.UserId)
		fmt.Println("Context Id:", messageRef.Parent.ContextId)
	case messagingrsn.OrganisationMessageParentType:
		fmt.Println("Organisation Id:", messageRef.Parent.OrganisationId)
	case messagingrsn.OrganisationSharedMailboxMessageParentType:
		fmt.Println("Organisation Id:", messageRef.Parent.OrganisationId)
		fmt.Println("SharedMailbox Id:", messageRef.Parent.SharedMailboxId)
	default:
		fmt.Println("Unknown MessageRef parent type")
	}

	outResourceName := messageRef.ResourceName()
	if resourceName != outResourceName {
		log.Fatalf("got %v, want %v", outResourceName, resourceName)
	}
}
